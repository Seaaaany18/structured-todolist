import { ref } from "vue";
import { useQuasar } from "quasar";
import { useRoute, useRouter } from "vue-router";

export default {
  setup() {
    const $q = useQuasar();
    const route = useRoute();
    const router = useRouter();

    let form = ref({
      fund_type_id: null,
      form_type: null,
    });

    let pageLoadingState = ref(false);

    let keyResults = ref([
      { task_name: null, time: null, showTimePicker: false },
    ]);

    let showAnyTimePicker = ref(false);

    const addKeyResult = () => {
      keyResults.value.push({
        task_name: null,
        time: null,
        showTimePicker: false,
      });
    };

    const removeKeyResult = (index) => {
      if (index > -1) {
        keyResults.value.splice(index, 1);
      }
    };

    const saveTime = (index) => {
      keyResults.value[index].showTimePicker = false;
      showAnyTimePicker.value = false; // Hide all time pickers
    };

    const cancelTime = (index) => {
      keyResults.value[index].showTimePicker = false;
      showAnyTimePicker.value = false; // Hide all time pickers
    };

    const showTimePicker = (index) => {
      keyResults.value.forEach((task, i) => {
        if (i !== index) {
          task.showTimePicker = false;
        }
      });
      keyResults.value[index].showTimePicker = true;
      showAnyTimePicker.value = true; // Show at least one time picker
    };

    const addNewRange = () => {
      // Here you can save the task details along with the title, task name, time, and date when created.
      // You can access form, keyResults, and other relevant data to save the task details.
      // For example:
      const newTask = {
        title: form.value.form_type,
        tasks: keyResults.value.map((task) => ({
          task_name: task.task_name,
          time: task.time,
          created_at: new Date().toISOString(), // Add current date and time when the task is created
        })),
      };
      console.log("New task:", newTask);
    };


        // for notification
        const showNotify = () => {
          let status = true;
          $q.notify({
            position: $q.screen.width < 767 ? "top" : "bottom-right",
            classes: `${
              status ? "onboarding-success-notif" : "onboarding-error-notif"
            } q-px-lg q-pt-none q-pb-none`,
            html: true,
            message: status
              ? `<div class="text-bold">Successfully Updated!</div> New To-Do List has been Updated successfully!`
              : `<div class="text-bold">Failed to Update!</div> New To-Do List has been Updated unsuccessfully!`,
          });
        };




    return {
      form,
      pageLoadingState,
      route,
      addKeyResult,
      removeKeyResult,
      keyResults,
      showAnyTimePicker,
      saveTime,
      cancelTime,
      showTimePicker,
      addNewRange,
      showNotify,
    };
  },
};
